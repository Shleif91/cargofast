(function($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 48)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 54
    });

    // Collapse the navbar when page is scrolled
    $(window).scroll(function() {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    });

    $("#getDate").click(function () {
        var $numAdv = $('#numAdv');
        var $phone = $('#phone');
        var countErrors = 0;

        if ($numAdv.val() === '' || $numAdv.val().length !== 5) {
            $numAdv.addClass('danger');
            countErrors++;
        }
        if ($phone.val() === '' || $phone.val().length < 9) {
            $phone.addClass('danger');
            countErrors++;
        }

        if (countErrors === 0) {
            $.post("http://cargofast.by/adminka/app/web/api/send-sms", {
                numberAdv: $numAdv.val(),
                phone: $phone.val()
            }).done(function (data) {
                $('#text').text(data);
                $('#infoModal').modal('show');

            }).fail(function (data) {
                $('#text').text(data.responseJSON);
                $('#infoModal').modal('show');
            });
        }
    })
})(jQuery);